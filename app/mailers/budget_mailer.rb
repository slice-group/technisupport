class BudgetMailer < ApplicationMailer
	def budget_notification(budget)
		@budget = budget
		mail(from: "gbrlmrllo@gmail.com", to: "gbrlmrllo@gmail.com", subject: "#{@budget.name.humanize} ha realizado una solicitud de cotización")
	end
end
