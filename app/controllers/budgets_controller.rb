#Generado con Keppler.
class BudgetsController < ApplicationController  
  before_filter :authenticate_user!, except: [:create]
  layout 'admin/application'
  load_and_authorize_resource except: [:create]
  before_action :set_budget, only: [:show, :edit, :update, :destroy]

  # GET /budgets
  def index
    budgets = Budget.searching(@query).all
    @objects, @total = budgets.page(@current_page), budgets.size
    redirect_to budgets_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /budgets/1
  def show
  end

  # GET /budgets/new
  def new
    @budget = Budget.new
  end

  # GET /budgets/1/edit
  def edit
  end

  # POST /budgets
  def create
    @budget = Budget.new(budget_params)
    notice = "#{@budget.name}, tu solicitud de cotizacion ha sido enviada correctamente."
    alert = "Por favor, asegurese de ingresar los datos de la cotización de manera correcta."

    if @budget.save
      BudgetMailer.budget_notification(@budget).deliver
      @budget.product ? redirect_to(category_products_path(@budget.product.category.permalink), notice: notice) : redirect_to(root_path, notice: notice)
    else
      
      @budget.product ? redirect_to(category_products_path(@budget.product.category.permalink), alert: alert) : redirect_to(root_path, alert: alert)
    end
  end

  # PATCH/PUT /budgets/1
  def update
    if @budget.update(budget_params)
      redirect_to @budget, notice: 'Budget was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /budgets/1
  def destroy
    @budget.destroy
    redirect_to budgets_url, notice: 'Budget was successfully destroyed.'
  end

  def destroy_multiple
    Budget.destroy redefine_ids(params[:multiple_ids])
    redirect_to budgets_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_budget
      @budget = Budget.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def budget_params
      params.require(:budget).permit(:email, :name, :city, :phone, :origanization, :description, :product_id)
    end
end
