#Generado con Keppler.
class MarksController < ApplicationController  
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_mark, only: [:show, :edit, :update, :destroy]

  # GET /marks
  def index
    marks = Mark.searching(@query).all
    @objects, @total = marks.page(@current_page), marks.size
    redirect_to marks_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /marks/1
  def show
  end

  # GET /marks/new
  def new
    @mark = Mark.new
  end

  # GET /marks/1/edit
  def edit
  end

  # POST /marks
  def create
    @mark = Mark.new(mark_params)

    if @mark.save
      redirect_to @mark, notice: 'Mark was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /marks/1
  def update
    if @mark.update(mark_params)
      redirect_to @mark, notice: 'Mark was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /marks/1
  def destroy
    @mark.destroy
    redirect_to marks_url, notice: 'Mark was successfully destroyed.'
  end

  def destroy_multiple
    Mark.destroy redefine_ids(params[:multiple_ids])
    redirect_to marks_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente" 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mark
      @mark = Mark.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def mark_params
      params.require(:mark).permit(:name)
    end
end
