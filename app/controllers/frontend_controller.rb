class FrontendController < ApplicationController
	layout :resolve_layout
	
  def index
  	@categories = Category.all
    @message = KepplerContactUs::Message.new
    @budget = Budget.new
  end

  # multifunctionals
  def products
  	@category = Category.find_by_permalink(params[:category])
    @message = KepplerContactUs::Message.new
    case @category.permalink
      when "multifuncionales"
        multifuncionales #private method
      when "scanners-profesionales"
        scanners(@category)
      when "plotters"
        plotters(@category)
      when "proyectores"
        proyectores(@category)
    end
  end

  def supplies
    @category = Category.find_by_permalink(params[:category])
    @message = KepplerContactUs::Message.new
    case @category.permalink
      when "suministros"
        suministros(@category) #private method
      when "repuestos"
        repuestos(@category)
      when "toners"
        toners(@category)
      when "otros"
        otros(@category)
    end    
  end

  def typeprints
    @message = KepplerContactUs::Message.new
    @mark = Mark.find_by_permalink(params[:mark])
    @stat = Stat.find_by_permalink(params[:stat])
    @typeprints = Typeprint.first(2)
  end

  def listing_multifunctionals
    @message = KepplerContactUs::Message.new
    @budget = Budget.new
    @mark = Mark.find_by_permalink(params[:mark])
    @stat = Stat.find_by_permalink(params[:stat])
    @typeprint = Typeprint.find_by_permalink(params[:typeprint])
    @products = Product.where(category: 1, mark_id: @mark.id, stat_id: @stat.id, typeprint_id: @typeprint.id).page(params[:page]).per(4)
  end
  # end

  private

  def resolve_layout
    case action_name
	    when "index"
	      "layouts/frontend/application"
	    when "products", "typeprints", "listing_multifunctionals", "supplies"
	      "layouts/frontend/products"
    end
  end

  def multifuncionales
    @marks = Mark.first(2)
    @stats = Stat.first(2)
    @budget = Budget.new
  end

  def scanners(category)
    @products = Product.where(category: category.id).page(params[:page]).per(4)
    @budget = Budget.new
  end

  def plotters(category)
    @products = Product.where(category: category.id).page(params[:page]).per(4)
    @budget = Budget.new
  end

  def proyectores(category)    
    @products = Product.where(category: category.id).page(params[:page]).per(4)
    @budget = Budget.new
  end

  def suministros(category)   
    @products = Product.where(category: category.id).page(params[:page]).per(4)
    @budget = Budget.new  
  end

  def repuestos(category)   
    @products = Product.where(category: category.id).page(params[:page]).per(4)
    @budget = Budget.new  
  end

  def toners(category)   
    @products = Product.where(category: category.id).page(params[:page]).per(4)
    @budget = Budget.new 
  end

  def otros(category) 
    @products = Product.where(category: category.id).page(params[:page]).per(4)
    @budget = Budget.new 
  end

end
