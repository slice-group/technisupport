module ApplicationHelper
	def title(page_title)
		content_for(:title) { page_title }
	end
	
	def	meta_description(page_description)
		content_for(:description) { page_description }
	end

	def loggedin?
		current_user
	end

	def is_index?
		action_name.to_sym.eql?(:index)
	end

	def model
		controller.controller_path.classify.constantize
	end

	def header_information(&block)
		content_for(:header_information) { capture(&block) }
	end

	def products_path?
		action_name.eql?("products") or action_name.eql?("typeprints") or action_name.eql?("listing_multifunctionals")
	end

	def supplies_path?
		action_name.eql?("supplies")
	end
end
