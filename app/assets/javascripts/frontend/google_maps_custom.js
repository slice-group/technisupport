var map;
var markersArray = [];
var latlng = new google.maps.LatLng(-0.180416,-78.487042);

function initialize()
{  
  var myOptions = {
    zoom: 15,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false
  };
  
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  placeMarker(latlng);
}

function placeMarker(location) {
    
  var marker = new google.maps.Marker({
    position: location, 
    map: map,
    animation: google.maps.Animation.BOUNCE,
    title:"Technisupport",
    icon: "/assets/frontend/favicon.png"
  });

  // add marker in markers array
  markersArray.push(marker);

  //map.setCenter(location);
}


google.maps.event.addDomListener(window, 'load', initialize);