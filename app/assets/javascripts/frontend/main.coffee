removeHash = ->
  history.pushState '', document.title, window.location.pathname + window.location.search
  return

$(document).on 'ready page:load', () -> 
  #one-page-nav
  $('.top-nav').onePageNav
    currentClass: 'current'
    changeHash: false
    scrollSpeed: 500
    filter: ':not(.external)'
    begin: ->
      removeHash()
      return
    end: ->
      
    scrollChange: ($currentListItem) ->
      return