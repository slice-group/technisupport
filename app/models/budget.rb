#Generado por keppler
require 'elasticsearch/model'
class Budget < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  belongs_to :product
  validates_presence_of :email, :name, :city, :phone, :description
  
  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      email:  self.email.to_s,
      name:  self.name.to_s,
      city:  self.city.to_s,
      phone:  self.phone.to_s,
      origanization:  self.origanization.to_s,
      description:  self.description.to_s,
      product_id:  self.product_id.to_s,
    }.as_json
  end

end
#Budget.import
