class Stat < ActiveRecord::Base
	has_many :products
	before_save :create_permalink, on: :create
	validates_uniqueness_of :name

	def create_permalink
    self.permalink = self.name.downcase.parameterize
  end
end
