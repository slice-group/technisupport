#Generado por keppler
require 'elasticsearch/model'
class Product < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  mount_uploader :image, ImageUploader
  belongs_to :category
  belongs_to :mark
  belongs_to :stat
  belongs_to :typeprint
  has_many :budget
  before_save :create_permalink, on: :create
  validates_uniqueness_of :name
  
  after_commit on: [:update] do
    puts __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:id, :name, :price, :category, :mark, :stat, :typeprint] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name: self.name,
      description: self.description,
      price: self.price.to_s,
      spacification_link: self.spacification_link,
      category: self.category.name,
      mark: self.mark.name,
      stat: self.stat.name,
      typeprint: self.typeprint.name,
    }.as_json
  end


  private

  def create_permalink
    self.permalink = self.name.downcase.parameterize
  end

end
#Product.import
