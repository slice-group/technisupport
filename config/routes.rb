Rails.application.routes.draw do

  root to: 'frontend#index'

  get "products/:category", to: "frontend#products", as: :category_products
  get "products/multifuncionales/:mark/:stat", to: "frontend#typeprints"
  get "products/multifuncionales/:mark/:stat/:typeprint", to: "frontend#listing_multifunctionals"
  get "supplies/:category", to: "frontend#supplies", as: :category_supplies


  devise_for :users, skip: KepplerConfiguration.skip_module_devise

  resources :admin, only: :index

  scope :admin do
   
  	resources :users do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :marks do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :products do 
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :budgets do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end
    
  end


  #errors
  match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all


  #dashboard
  mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'

  #message
  mount KepplerContactUs::Engine, :at => '/', as: 'messages'

  #adwords
  mount KepplerAdwords::Engine, :at => '/', as: 'adwords'

  #adword
  #mount KepplerAdwords::Engine, :at => '/', as: 'adwords'

end
