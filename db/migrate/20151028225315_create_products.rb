class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :image
      t.string :name
      t.string :permalink
      t.text :description
      t.text :attributes_description
      t.float :price
      t.string :spacification_link
      t.integer :category_id
      t.integer :mark_id
      t.integer :stat_id
      t.integer :typeprint_id

      t.timestamps null: false
    end
  end
end
