class CreateTypeprints < ActiveRecord::Migration
  def change
    create_table :typeprints do |t|
      t.string :name
      t.string :permalink

      t.timestamps null: false
    end
  end
end
