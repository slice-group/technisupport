# This migration comes from keppler_adwords (originally 20151123140154)
class CreateKepplerAdwordsMetaTags < ActiveRecord::Migration
  def change
    create_table :keppler_adwords_meta_tags do |t|
      t.string :url
      t.string :title
      t.text :description

      t.timestamps null: false
    end
  end
end
