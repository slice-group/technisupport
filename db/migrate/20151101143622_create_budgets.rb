class CreateBudgets < ActiveRecord::Migration
  def change
    create_table :budgets do |t|
      t.string :email
      t.string :name
      t.string :city
      t.string :phone
      t.string :origanization
      t.text :description
      t.integer :product_id

      t.timestamps null: false
    end
  end
end
